#include "palabos2D.h"
#include "palabos2D.hh"
#include "headers.h"
#include "headers.hh"
#include <cstdlib>
#include <iostream>
#include <random>

using namespace plb;
using namespace std;

typedef double T;

#define NSDESCRIPTOR descriptors::ForcedD2Q9Descriptor
#define NSDYNAMICS GuoExternalForceBGKdynamics

/// Initialization of the volume fraction field.
template<typename T, template<typename NSU> class nsDescriptor>
struct IniVolFracProcessor2D : public BoxProcessingFunctional2D_S<T>
{
    IniVolFracProcessor2D(plint ny_)
        : ny(ny_)
    { }
    virtual void process(Box2D domain, ScalarField2D<T>& volfracField)
    {
        Dot2D absoluteOffset = volfracField.getLocation();
        T up=0.135;
        T low=0.25;
            for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
                for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                      plint absoluteY = absoluteOffset.y + iY;
                        	T VolFrac;
                          std::random_device rd;
                          std::mt19937 mt(rd());
                          std::normal_distribution<> dist(1, 0.01);
                          T rand_val = dist(mt);

    	      if(absoluteY>(ny-1)-util::roundToInt(((up/(up+low))*ny))) VolFrac=rand_val;
            else VolFrac = 0.;

            volfracField.get(iX, iY) = VolFrac;

            }
        }
    }

    virtual IniVolFracProcessor2D<T,nsDescriptor>* clone() const
    {
        return new IniVolFracProcessor2D<T,nsDescriptor>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
private :
    plint ny;
};

/// Initialization of the density field.
template<typename T, template<typename NSU> class nsDescriptor>
struct IniDensityProcessor2D : public BoxProcessingFunctional2D_S<T>
{
    IniDensityProcessor2D(plint ny_)
        : ny(ny_)
    { }
    virtual void process(Box2D domain, ScalarField2D<T>& densityfield)
    {
        Dot2D absoluteOffset = densityfield.getLocation();
        T up=0.135;
        T low=0.25;
            for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
                for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                      plint absoluteY = absoluteOffset.y + iY;
                        	T dens;
                          if(absoluteY>(ny-1)-util::roundToInt(((up/(up+low))*ny))) dens=1000.0;
                          else dens = 1008.4;
                          densityfield.get(iX, iY) = dens;
            }
        }
    }

    virtual IniDensityProcessor2D<T,nsDescriptor>* clone() const
    {
        return new IniDensityProcessor2D<T,nsDescriptor>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
private :
    plint ny;
};


void ExpSetup (
        MultiBlockLattice2D<T, NSDESCRIPTOR>& nsLattice,
        MultiScalarField2D<T>& volfracField,
        MultiScalarField2D<T>& densityField)
{
    const plint ny = nsLattice.getNy();
    initializeAtEquilibrium(nsLattice, nsLattice.getBoundingBox(), (T)1., Array<T,2>((T)0.,(T)0.) );

    applyProcessingFunctional(new IniVolFracProcessor2D<T,NSDESCRIPTOR>(ny), volfracField.getBoundingBox(), volfracField);
    applyProcessingFunctional(new IniDensityProcessor2D<T,NSDESCRIPTOR>(ny), densityField.getBoundingBox(), densityField );

    nsLattice.initialize();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template< typename T, template<typename U> class FluidDescriptor>
  class AdvectionDiffusionFd2D : public BoxProcessingFunctional2D
  {

  public:

      AdvectionDiffusionFd2D(T D_);
      virtual void processGenericBlocks(Box2D domain, std::vector<AtomicBlock2D*> fields );
      virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
      virtual AdvectionDiffusionFd2D<T, FluidDescriptor>* clone() const;
    private:
      T D;

  };

template< typename T, template<typename U> class FluidDescriptor>
AdvectionDiffusionFd2D<T,FluidDescriptor>::AdvectionDiffusionFd2D(T D_) : D(D_)
{ }


template< typename T, template<typename U> class FluidDescriptor>
void AdvectionDiffusionFd2D<T,FluidDescriptor>::processGenericBlocks (
        Box2D domain, std::vector<AtomicBlock2D*> fields )
{

    PLB_PRECONDITION(fields.size()==5);
    ScalarField2D<T>* phi_t    = dynamic_cast<ScalarField2D<T>*>(fields[0]);
    ScalarField2D<T>* phi_tp1  = dynamic_cast<ScalarField2D<T>*>(fields[1]);
    ScalarField2D<T>* result   = dynamic_cast<ScalarField2D<T>*>(fields[2]);
    ScalarField2D<T>* Q        = dynamic_cast<ScalarField2D<T>*>(fields[3]);
    BlockLattice2D<T, FluidDescriptor>* fluid = dynamic_cast<BlockLattice2D<T, FluidDescriptor>*>(fields[4]);

    Dot2D ofs1 = computeRelativeDisplacement(*phi_t, *phi_tp1);
    Dot2D ofs2 = computeRelativeDisplacement(*phi_t, *result);
    Dot2D ofs3 = computeRelativeDisplacement(*phi_t, *Q);
    Dot2D ofs4 = computeRelativeDisplacement(*phi_t, *fluid);


    for (plint iX=domain.x0; iX<=domain.x1; ++iX)
    {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY)
        {

           T phiC = phi_tp1->get(iX  +ofs1.x, iY  +ofs1.y);
           T phiE = phi_tp1->get(iX+1+ofs1.x, iY  +ofs1.y);
           T phiW = phi_tp1->get(iX-1+ofs1.x, iY  +ofs1.y);
           T phiN = phi_tp1->get(iX  +ofs1.x, iY+1+ofs1.y);
           T phiS = phi_tp1->get(iX  +ofs1.x, iY-1+ofs1.y);

           Array<T,2> vel;
           fluid->get(iX+ofs4.x,iY+ofs4.y).computeVelocity(vel);

           Array<T, 2> adv;
           T diffX, diffY;

           adv[0] = (util::greaterThan(vel[0], (T) 0) ? (phiC - phiW) : (util::lessThan(vel[0], (T) 0) ? (phiE - phiC) : (T) 0.5 * (phiE - phiW)));
           adv[1] = (util::greaterThan(vel[1], (T) 0) ? (phiC - phiS) : (util::lessThan(vel[1], (T) 0) ? (phiN - phiC) : (T) 0.5 * (phiN - phiS)));

           diffX = phiW + phiE - (T) 2 * phiC;
           diffY = phiS + phiN - (T) 2 * phiC;

           T advection = vel[0] * adv[0] + vel[1] * adv[1];
           T diffusion = D * (diffX + diffY);

           result->get(iX+ofs2.x,iY+ofs2.y) = phi_t->get(iX,iY) + diffusion - advection + Q->get(iX+ofs3.x,iY+ofs3.y);

        }
    }
}

template< typename T, template<typename U> class FluidDescriptor>
AdvectionDiffusionFd2D<T,FluidDescriptor>*
    AdvectionDiffusionFd2D<T,FluidDescriptor>::clone() const
{
    return new AdvectionDiffusionFd2D<T,FluidDescriptor>(*this);
}

template< typename T, template<typename U> class FluidDescriptor>
void AdvectionDiffusionFd2D<T,FluidDescriptor>::getTypeOfModification(std::vector<modif::ModifT>& modified) const {
    modified[0] = modif::nothing;
    modified[1] = modif::nothing;
    modified[2] = modif::staticVariables;
    modified[3] = modif::nothing;
    modified[4] = modif::nothing;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// WENO 3rd order procedure
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template< typename T, template<typename U> class FluidDescriptor>
  class AdvectionDiffusionFd2D_WENO3 : public BoxProcessingFunctional2D
  {

  public:

      AdvectionDiffusionFd2D_WENO3(Array<T,2> vsed_, T D_, T epsilon_);
      virtual void processGenericBlocks(Box2D domain, std::vector<AtomicBlock2D*> fields );
      virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
      virtual AdvectionDiffusionFd2D_WENO3<T, FluidDescriptor>* clone() const;
    private:
      Array<T,2> vsed;
      T D, epsilon;

  };

template< typename T, template<typename U> class FluidDescriptor>
AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>::AdvectionDiffusionFd2D_WENO3(Array<T,2> vsed_, T D_, T epsilon_) : vsed(vsed_), D(D_), epsilon(epsilon_)
{ }


template< typename T, template<typename U> class FluidDescriptor>
void AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>::processGenericBlocks (
        Box2D domain, std::vector<AtomicBlock2D*> fields )
{

    PLB_PRECONDITION(fields.size()==5);
    ScalarField2D<T>* phi_t    = dynamic_cast<ScalarField2D<T>*>(fields[0]);
    ScalarField2D<T>* phi_tp1  = dynamic_cast<ScalarField2D<T>*>(fields[1]);
    ScalarField2D<T>* result   = dynamic_cast<ScalarField2D<T>*>(fields[2]);
    ScalarField2D<T>* Q        = dynamic_cast<ScalarField2D<T>*>(fields[3]);
    BlockLattice2D<T, FluidDescriptor>* fluid = dynamic_cast<BlockLattice2D<T, FluidDescriptor>*>(fields[4]);

    Dot2D ofs1 = computeRelativeDisplacement(*phi_t, *phi_tp1);
    Dot2D ofs2 = computeRelativeDisplacement(*phi_t, *result);
    Dot2D ofs3 = computeRelativeDisplacement(*phi_t, *Q);
    Dot2D ofs4 = computeRelativeDisplacement(*phi_t, *fluid);


    for (plint iX=domain.x0; iX<=domain.x1; ++iX)
    {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY)
        {

           T phiC = phi_tp1->get(iX  +ofs1.x, iY  +ofs1.y);
           T phiE1 = phi_tp1->get(iX+1+ofs1.x, iY  +ofs1.y);
           T phiW1 = phi_tp1->get(iX-1+ofs1.x, iY  +ofs1.y);
           T phiN1 = phi_tp1->get(iX  +ofs1.x, iY+1+ofs1.y);
           T phiS1 = phi_tp1->get(iX  +ofs1.x, iY-1+ofs1.y);

           T phiE2 = phi_tp1->get(iX+2+ofs1.x, iY  +ofs1.y);
           T phiW2 = phi_tp1->get(iX-2+ofs1.x, iY  +ofs1.y);
           T phiN2 = phi_tp1->get(iX  +ofs1.x, iY+2+ofs1.y);
           T phiS2 = phi_tp1->get(iX  +ofs1.x, iY-2+ofs1.y);

           Array<T,2> vel;
           fluid->get(iX+ofs4.x,iY+ofs4.y).computeVelocity(vel);

           Array<T, 2> adv;
           Array<T, 2> fp_p12;
           Array<T, 2> fp_n12;

           Array<T, 2> fp_p12_1;
           Array<T, 2> fp_p12_2;
           Array<T, 2> fp_n12_1;
           Array<T, 2> fp_n12_2;

           Array<T, 2> bp_p12_1;
           Array<T, 2> bp_p12_2;
           Array<T, 2> bp_n12_1;
           Array<T, 2> bp_n12_2;

           Array<T, 2> alpha_p_p12_1;
           Array<T, 2> alpha_p_p12_2;
           Array<T, 2> alpha_p_n12_1;
           Array<T, 2> alpha_p_n12_2;

           Array<T, 2> w1p_p12;
           Array<T, 2> w2p_p12;
           Array<T, 2> w1p_n12;
            Array<T, 2> w2p_n12;
           T diffX, diffY;

           if (util::greaterThan(vel[0], (T) 0)) {
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_p12x
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             fp_p12_1[0] = (phiC + phiE1)/(T)2;
             fp_p12_2[0] = -(phiW1 - (T)3 * phiC)/(T)2;

             bp_p12_1[0] = (phiE1 - phiC)*(phiE1 - phiC);
             bp_p12_2[0] = (phiC - phiW1)*(phiC - phiW1);

           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_n12x
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             fp_n12_1[0] = (phiW1 + phiC)/(T)2;
             fp_n12_2[0] = -(phiW2 - (T)3*phiW1)/(T)2;

             bp_n12_1[0] = (phiC - phiW1)*(phiC - phiW1);
             bp_n12_2[0] = (phiW1 - phiW2)*(phiW1 - phiW2);

           /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


           } else {

             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             //  fp_p12x
             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
               fp_p12_1[0] = -(phiE2 - (T)3*phiE1)/(T)2;
               fp_p12_2[0] = (phiE1 + phiC)/(T)2;

               bp_p12_1[0] = (phiE1 - phiE2)*(phiE1 - phiE2);
               bp_p12_2[0] = (phiC - phiE1)*(phiC - phiE1);

           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_n12x
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             fp_n12_1[0] = -(phiE1 - (T)3*phiC)/(T)2;
             fp_n12_2[0] = (phiC + phiW1)/(T)2;

             bp_n12_1[0] = (phiC - phiE1)*(phiC - phiE1);
             bp_n12_2[0] = (phiW1 - phiC)*(phiW1 - phiC);

           /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             // adv[0] = fp_p12x - fp_n12x;


           }

           if (util::greaterThan(vel[1]+vsed[1], (T) 0)) {
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_p12y
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

             fp_p12_1[1] = (phiC + phiN1)/(T)2;
             fp_p12_2[1] = -(phiS1 - (T)3 * phiC)/(T)2;


             bp_p12_1[1] = (phiN1 - phiC)*(phiN1 - phiC);
             bp_p12_2[1] = (phiC - phiS1)*(phiC - phiS1);

           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_n12y
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           fp_n12_1[1] = (phiS1 + phiC)/(T)2;
           fp_n12_2[1] = -(phiS2 - (T)3*phiS1)/(T)2;


           bp_n12_1[1] = (phiC - phiS1)*(phiC - phiS1);
           bp_n12_2[1] = (phiS1 - phiS2)*(phiS1 - phiS2);


           /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             // adv[1] = fp_p12y - fp_n12y;
           } else {


           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_p12y
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           fp_p12_1[1] = -(phiN2 - (T)3*phiN1)/(T)2;
           fp_p12_2[1] = (phiN1 + phiC)/(T)2;

           bp_p12_1[1] = (phiN1 - phiN2)*(phiN1 - phiN2);
           bp_p12_2[1] = (phiC - phiN1)*(phiC - phiN1);

           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_n12y
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

           fp_n12_1[1] = -(phiN1 - (T)3*phiC)/(T)2;
           fp_n12_2[1] = (phiC + phiS1)/(T)2;

           bp_n12_1[1] = (phiC - phiN1)*(phiC - phiN1);
           bp_n12_2[1] = (phiS1 - phiC)*(phiS1 - phiC);


           }

      for(plint i = 0; i <= 1; i++){

        alpha_p_p12_1[i] = (T)2 / ((T)3 * (epsilon + bp_p12_1[i])*(epsilon + bp_p12_1[i]));
        alpha_p_p12_2[i] = (T)1 / ((T)3 * (epsilon + bp_p12_2[i])*(epsilon + bp_p12_2[i]));
        alpha_p_n12_1[i] = (T)2 / ((T)3 * (epsilon + bp_n12_1[i])*(epsilon + bp_n12_1[i]));
        alpha_p_n12_2[i] = (T)1 / ((T)3 * (epsilon + bp_n12_2[i])*(epsilon + bp_n12_2[i]));


        w1p_p12[i] = alpha_p_p12_1[i] / (alpha_p_p12_1[i] + alpha_p_p12_2[i]);
        w2p_p12[i] = alpha_p_p12_2[i] / (alpha_p_p12_1[i] + alpha_p_p12_2[i]);
        w1p_n12[i] = alpha_p_n12_1[i] / (alpha_p_n12_1[i] + alpha_p_n12_2[i]);
        w2p_n12[i] = alpha_p_n12_2[i] / (alpha_p_n12_1[i] + alpha_p_n12_2[i]);


        fp_p12[i] = w1p_p12[i] * fp_p12_1[i] + w2p_p12[i] * fp_p12_2[i];
        fp_n12[i] = w1p_n12[i] * fp_n12_1[i] + w2p_n12[i] * fp_n12_2[i];

      }

      adv = fp_p12 - fp_n12;



      diffX = phiW1 + phiE1 - (T) 2 * phiC;
      diffY = phiS1 + phiN1 - (T) 2 * phiC;

      T advection = vel[0] * adv[0] + (vel[1]+vsed[1]) * adv[1];
      T diffusion = D * (diffX + diffY);

      result->get(iX+ofs2.x,iY+ofs2.y) = diffusion - advection + Q->get(iX+ofs3.x,iY+ofs3.y);

        }
    }
}

template< typename T, template<typename U> class FluidDescriptor>
AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>*
    AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>::clone() const
{
    return new AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>(*this);
}

template< typename T, template<typename U> class FluidDescriptor>
void AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>::getTypeOfModification(std::vector<modif::ModifT>& modified) const {
    modified[0] = modif::nothing;
    modified[1] = modif::nothing;
    modified[2] = modif::staticVariables;
    modified[3] = modif::nothing;
    modified[4] = modif::nothing;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Force Term
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template< typename T, template<typename U> class FluidDescriptor>
class ScalarBuoyanTermProcessor2D :  public BoxProcessingFunctional2D
{
public:

    ScalarBuoyanTermProcessor2D(T gravity_, T rho0_, T rhoP_, T TotalVolFrac_, T dt_,
                                 Array<T,FluidDescriptor<T>::d> dir_);

    virtual void processGenericBlocks( Box2D domain, std::vector<AtomicBlock2D*> fields );
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual ScalarBuoyanTermProcessor2D<T,FluidDescriptor>* clone() const;

private:
    T gravity, rho0, rhoP, TotalVolFrac, dt;
    Array<T,FluidDescriptor<T>::d> dir;
};

template< typename T, template<typename U> class FluidDescriptor>
ScalarBuoyanTermProcessor2D<T,FluidDescriptor>::
        ScalarBuoyanTermProcessor2D(T gravity_, T rho0_, T rhoP_, T TotalVolFrac_, T dt_, Array<T,FluidDescriptor<T>::d> dir_)
    :  gravity(gravity_), rho0(rho0_), rhoP(rhoP_), TotalVolFrac(TotalVolFrac_), dt(dt_),
       dir(dir_)
{
    // We normalize the direction of the force vector.
    T normDir = std::sqrt(VectorTemplate<T,FluidDescriptor>::normSqr(dir));
    for (pluint iD = 0; iD < FluidDescriptor<T>::d; ++iD) {
        dir[iD] /= normDir;
    }
}


template< typename T, template<typename U> class FluidDescriptor >
void ScalarBuoyanTermProcessor2D<T,FluidDescriptor>::processGenericBlocks (
        Box2D domain, std::vector<AtomicBlock2D*> fields )
{
    typedef FluidDescriptor<T> D;
    enum {
        forceOffset = FluidDescriptor<T>::ExternalField::forceBeginsAt
    };

    PLB_PRECONDITION(fields.size()==3);
    BlockLattice2D<T, FluidDescriptor>* fluid = dynamic_cast<BlockLattice2D<T, FluidDescriptor>*>(fields[0]);
    ScalarField2D<T>* volfracfield = dynamic_cast<ScalarField2D<T>*>(fields[1]);
    ScalarField2D<T>* densityfield = dynamic_cast<ScalarField2D<T>*>(fields[2]);


    Dot2D offset1 = computeRelativeDisplacement(*fluid, *volfracfield);
    Dot2D offset2 = computeRelativeDisplacement(*fluid, *densityfield);


    Array<T,D::d> gravOverrho0 (
            gravity*dir[0]/rho0,
            gravity*dir[1]/rho0);


    T maxiT = 0.1/dt;
    T iT = fluid->getTimeCounter().getTime();
    T gain = util::sinIncreasingFunction(iT, maxiT);
    // The gain allows to integrate the force term smoothly in time (useful to soften the waves in the fluid velocity field)

    for (plint iX=domain.x0; iX<=domain.x1; ++iX)
    {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY)
        {
                T localVolfrac = volfracfield->get(iX+offset1.x,iY+offset1.y);
                // Computation of the Boussinesq force
                T *force = fluid->get(iX,iY).getExternal(forceOffset);
                T dens = densityfield->get(iX+offset2.x,iY+offset2.y);
                const T diffT = rhoP-rho0;
                for (pluint iD = 0; iD < D::d; ++iD)
                {
			                 force[iD] = - gain * gravOverrho0[iD] * (diffT * localVolfrac * TotalVolFrac + (dens - rho0)*(1 - localVolfrac*TotalVolFrac));
                }
          }
    }
}

template< typename T, template<typename U> class FluidDescriptor>
ScalarBuoyanTermProcessor2D<T,FluidDescriptor>*
    ScalarBuoyanTermProcessor2D<T,FluidDescriptor>::clone() const
{
    return new ScalarBuoyanTermProcessor2D<T,FluidDescriptor>(*this);
}

template< typename T, template<typename U> class FluidDescriptor>
void ScalarBuoyanTermProcessor2D<T,FluidDescriptor>::getTypeOfModification(std::vector<modif::ModifT>& modified) const {
    modified[0] = modif::staticVariables;
    modified[1] = modif::nothing;
    modified[2] = modif::nothing;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void writeVTK(MultiBlockLattice2D<T,NSDESCRIPTOR>& nsLattice,
              MultiScalarField2D<T>& volfracField, MultiScalarField2D<T>& densityField, plint iter, T dx, T dt)
{


    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 6), dx);
    vtkOut.writeData<float>(volfracField, "VolFrac", (T)1);
    vtkOut.writeData<float>(densityField, "dens", (T)1);

}

void writeGif(MultiBlockLattice2D<T,NSDESCRIPTOR>& nsLattice,
              MultiScalarField2D<T>& volfracField, MultiScalarField2D<T>& densityField, int iT)
{
    const plint imSize = 600;
    const plint nx = nsLattice.getNx();
    const plint ny = nsLattice.getNy();
    Box2D slice(0, nx-1, 0, ny-1);
    ImageWriter<T> imageWriter("leeloo.map");

    imageWriter.writeScaledGif(createFileName("VolFrac", iT, 6),
                               *extractSubDomain(volfracField, slice),
                               imSize, imSize);

    imageWriter.writeScaledGif(createFileName("Dens", iT, 6),
                               *extractSubDomain(densityField, slice),
                               imSize, imSize);
}


int main(int argc, char *argv[])
{
    plbInit(&argc, &argv);
    global::timer("simTime").start();


    const T lx  = 0.303;
    const T ly  = 0.385;
    const T uLb = 0.01;
    const T D = 2e-9;
    const T nu = 3e-6;
    const T epsilon = 1.5e-5;
    const T TotalVolFrac = 0.005;
    const T g = 9.81;
    const T rho0 = 1000.;
    const T rhoP = 2500.;

    Array<T,2> vsed(0., -0.0001);

    const plint resolution = 250;

    global::directories().setOutputDir("./tmp/");

    T dx = 1./resolution;
    T dt = uLb * dx;

    T g_lb = g * (dt*dt/dx);
    plint nx =  (lx/dx+(T)0.5) + 1;
    plint ny =  (ly/dx+(T)0.5) + 1;
    T nsTau = NSDESCRIPTOR<T>::invCs2*(dt*dt/(dx*dx))*nu + (dt/2);


    T nsOmega = dt/nsTau;

    Dynamics<T,NSDESCRIPTOR>* nsdynamics = new NSDYNAMICS<T,NSDESCRIPTOR>(nsOmega);
    MultiBlockLattice2D<T,NSDESCRIPTOR> nsLattice(nx,ny, nsdynamics->clone());
    defineDynamics(nsLattice, nsLattice.getBoundingBox(), nsdynamics->clone());
    delete nsdynamics; nsdynamics = 0;


    SparseBlockStructure2D blockStructure(createRegularDistribution2D(nx, ny));
    plint envelopeWidth = 2;
    MultiScalarField2D<T> volfracField(MultiBlockManagement2D(blockStructure, defaultMultiBlockPolicy2D().getThreadAttribution(), envelopeWidth ),
    defaultMultiBlockPolicy2D().getBlockCommunicator(),
    defaultMultiBlockPolicy2D().getCombinedStatistics(),
    defaultMultiBlockPolicy2D().getMultiScalarAccess<T>(), T());

    MultiScalarField2D<T> phi_t(volfracField), phi_tp1(volfracField), Q(volfracField), phi_n_adv(volfracField), phi_1(volfracField), phi_1_adv(volfracField), phi_2(volfracField), phi_2_adv(volfracField), volfracField_RK(volfracField);
    MultiScalarField2D<T> densityField(volfracField), D_t(volfracField), D_tp1(volfracField), Q_d(volfracField);

    nsLattice.periodicity().toggleAll(true);
    volfracField.periodicity().toggleAll(true);
    densityField.periodicity().toggleAll(true);
    phi_t.periodicity().toggleAll(true);
    phi_tp1.periodicity().toggleAll(true);
    Q.periodicity().toggleAll(true);
    D_t.periodicity().toggleAll(true);
    D_tp1.periodicity().toggleAll(true);
    Q_d.periodicity().toggleAll(true);

    Box2D bottom(0,nx-1,0,0);
    Box2D top(0,nx-1,ny-1,ny-1);
    Dynamics<T,NSDESCRIPTOR>* nsbbDynamics = new BounceBack<T,NSDESCRIPTOR>(0.);
    defineDynamics(nsLattice, bottom, nsbbDynamics->clone());
    defineDynamics(nsLattice, top, nsbbDynamics->clone());
    delete nsbbDynamics; nsbbDynamics = 0;

    nsLattice.toggleInternalStatistics(false);


    ExpSetup(nsLattice, volfracField, densityField);


    Array<T,NSDESCRIPTOR<T>::d> forceOrientation(T(), (T)1);


    std::vector<MultiBlock2D*> args_f;
    args_f.push_back(&nsLattice);
    args_f.push_back(&volfracField);
    args_f.push_back(&densityField);

    integrateProcessingFunctional (
            new ScalarBuoyanTermProcessor2D<T,NSDESCRIPTOR> (
                g_lb, rho0,
                rhoP, TotalVolFrac, dt, forceOrientation),
            nsLattice.getBoundingBox(),
            args_f, 0);


    T tIni = global::timer("simTime").stop();
    pcout << "time elapsed for ExpSetup:" << tIni << endl;
    global::timer("simTime").start();

    plint evalTime =5000;
    plint iT = 0;
    plint maxT = 8/dt;
    // plint maxT = 1;
    plint saveIter = 0.05/dt;
    util::ValueTracer<T> converge((T)1,(T)100,1.0e-3);

    pcout << "Max Number of iterations: " << maxT << endl;
    pcout << "Number of saving iterations: " << saveIter << endl;


    for (iT = 0; iT <= maxT; ++iT)
    {

        if (iT == (evalTime))
        {
            T tEval = global::timer("simTime").stop();
            T remainTime = (tEval - tIni) / (T)evalTime * (T)maxT/(T)3600;
            global::timer("simTime").start();
            pcout << "Remaining " << (plint)remainTime << " hours, and ";
            pcout << (plint)((T)60*(remainTime - (T)((plint)remainTime))+0.5) << " minutes." << endl;
        }

        if (iT % saveIter == 0)
        {

            pcout << iT * dt << " : Writing VTK." << endl;
            writeVTK(nsLattice, volfracField, densityField, iT, dx, dt);

            pcout << iT * dt << " : Writing gif." << endl;
            writeGif(nsLattice,volfracField, densityField, iT);


        }

        nsLattice.collideAndStream();

        plb::copy(densityField, D_t, densityField.getBoundingBox());
        plb::copy(densityField, D_tp1, densityField.getBoundingBox());
        std::vector<MultiBlock2D*> args_d;
        args_d.push_back(&D_t);
        args_d.push_back(&D_tp1);
        args_d.push_back(&densityField);
        args_d.push_back(&Q);
        args_d.push_back(&nsLattice);

        applyProcessingFunctional (new AdvectionDiffusionFd2D<T,NSDESCRIPTOR> (D*(dt/(dx*dx))), nsLattice.getBoundingBox(), args_d);

        plb::copy(volfracField, phi_t, volfracField.getBoundingBox());
        plb::copy(volfracField, phi_tp1, volfracField.getBoundingBox());
        std::vector<MultiBlock2D*> args1;
        args1.push_back(&phi_t);
        args1.push_back(&phi_tp1);
        args1.push_back(&phi_n_adv);
        args1.push_back(&Q);
        args1.push_back(&nsLattice);
        applyProcessingFunctional (new AdvectionDiffusionFd2D_WENO3<T,NSDESCRIPTOR> (vsed, D*(dt/(dx*dx)), epsilon), nsLattice.getBoundingBox(), args1);
        std::vector<MultiBlock2D*> arg_RK1;
        arg_RK1.push_back(&volfracField);
        arg_RK1.push_back(&phi_n_adv);
        arg_RK1.push_back(&phi_1);
        applyProcessingFunctional (new RK3_Step1_functional2D<T> (), nsLattice.getBoundingBox(), arg_RK1);

        plb::copy(phi_1, phi_t, volfracField.getBoundingBox());
        plb::copy(phi_1, phi_tp1, volfracField.getBoundingBox());
        std::vector<MultiBlock2D*> args2;
        args2.push_back(&phi_t);
        args2.push_back(&phi_tp1);
        args2.push_back(&phi_1_adv);
        args2.push_back(&Q);
        args2.push_back(&nsLattice);
        applyProcessingFunctional (new AdvectionDiffusionFd2D_WENO3<T,NSDESCRIPTOR> (vsed, D*(dt/(dx*dx)), epsilon), nsLattice.getBoundingBox(), args2);
        std::vector<MultiBlock2D*> arg_RK2;
        arg_RK2.push_back(&volfracField);
        arg_RK2.push_back(&phi_1_adv);
        arg_RK2.push_back(&phi_2);
        applyProcessingFunctional (new RK3_Step1_functional2D<T> (), nsLattice.getBoundingBox(), arg_RK2);

        plb::copy(phi_2, phi_t, volfracField.getBoundingBox());
        plb::copy(phi_2, phi_tp1, volfracField.getBoundingBox());
        std::vector<MultiBlock2D*> args3;
        args3.push_back(&phi_t);
        args3.push_back(&phi_tp1);
        args3.push_back(&phi_2_adv);
        args3.push_back(&Q);
        args3.push_back(&nsLattice);
        applyProcessingFunctional (new AdvectionDiffusionFd2D_WENO3<T,NSDESCRIPTOR> (vsed, D*(dt/(dx*dx)), epsilon), nsLattice.getBoundingBox(), args3);
        std::vector<MultiBlock2D*> arg_RK3;
        arg_RK3.push_back(&volfracField);
        arg_RK3.push_back(&phi_2_adv);
        arg_RK3.push_back(&volfracField_RK);
        applyProcessingFunctional (new RK3_Step1_functional2D<T> (), nsLattice.getBoundingBox(), arg_RK3);
        plb::copy(volfracField_RK, volfracField, volfracField.getBoundingBox());

    }

    writeGif(nsLattice,volfracField, densityField, iT);

    T tEnd = global::timer("simTime").stop();

    T totalTime = tEnd-tIni;
    T nx100 = nsLattice.getNx()/(T)100;
    T ny100 = nsLattice.getNy()/(T)100;
    pcout << "N=" << resolution << endl;
    pcout << "number of processors: " << global::mpi().getSize() << endl;
    pcout << "simulation time: " << totalTime << endl;
    pcout << "total time: " << tEnd << endl;
    pcout << "total iterations: " << iT << endl;
    pcout << "Msus: " << nx100*ny100*ny100*(T)iT/totalTime << endl;

    return 0;
}
