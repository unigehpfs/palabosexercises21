#ifndef RK3_HH
#define RK3_HH

#include "RK3.h"
#include "atomicBlock/blockLattice2D.h"
#include "core/util.h"
#include "finiteDifference/finiteDifference2D.h"
#include "latticeBoltzmann/geometricOperationTemplates.h"

namespace plb {

/* ******** RK3_Step1_functional2D ****************************************** */

template<typename T>
void RK3_Step1_functional2D<T>::process (
        Box2D domain, std::vector<ScalarField2D<T>*> fields )
{
    PLB_PRECONDITION( fields.size() == 3 );
    ScalarField2D<T>& phi_n = *fields[0];
    ScalarField2D<T>& phi_n_adv = *fields[1];
    ScalarField2D<T>&  phi_1 = *fields[2];
    Dot2D offset_phi_n_adv  = computeRelativeDisplacement(phi_n, phi_n_adv);
    Dot2D offset_phi_1      = computeRelativeDisplacement(phi_n, phi_1);
    for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY) {

                phi_1.get(iX+offset_phi_1.x,iY+offset_phi_1.y)
                    = phi_n.get(iX,iY) +
                      phi_n_adv.get(iX+offset_phi_n_adv.x,iY+offset_phi_n_adv.y);

        }
    }
}

template<typename T>
RK3_Step1_functional2D<T>* RK3_Step1_functional2D<T>::clone() const {
    return new RK3_Step1_functional2D<T>(*this);
}

template<typename T>
void RK3_Step1_functional2D<T>::getTypeOfModification(std::vector<modif::ModifT>& modified) const {
    modified[0] = modif::nothing; // phi_n
    modified[1] = modif::nothing; // phi_n_adv
    modified[2] = modif::staticVariables; // phi_1
}

template<typename T>
BlockDomain::DomainT RK3_Step1_functional2D<T>::appliesTo() const {
    return BlockDomain::bulkAndEnvelope; // Everything is local, no communication needed.
}


/* ******** RK3_Step2_functional2D ****************************************** */

template<typename T>
void RK3_Step2_functional2D<T>::process (
        Box2D domain, std::vector<ScalarField2D<T>*> fields )
{
    PLB_PRECONDITION( fields.size() == 4 );
    ScalarField2D<T>& phi_n = *fields[0];
    ScalarField2D<T>& phi_1 = *fields[1];
    ScalarField2D<T>& phi_1_adv = *fields[2];
    ScalarField2D<T>& phi_2 = *fields[3];
    Dot2D offset_phi_1      = computeRelativeDisplacement(phi_n, phi_1);
    Dot2D offset_phi_1_adv  = computeRelativeDisplacement(phi_n, phi_1_adv);
    Dot2D offset_phi_2      = computeRelativeDisplacement(phi_n, phi_2);
    for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY) {

                phi_2.get(iX+offset_phi_2.x,iY+offset_phi_2.y)
                    = 3./4. * phi_n.get(iX,iY) +
                      1./4. * phi_1.get(iX+offset_phi_1.x,iY+offset_phi_1.y) +
                      1./4. * phi_1_adv.get(iX+offset_phi_1_adv.x,iY+offset_phi_1_adv.y);

        }
    }
}

template<typename T>
RK3_Step2_functional2D<T>* RK3_Step2_functional2D<T>::clone() const {
    return new RK3_Step2_functional2D<T>(*this);
}

template<typename T>
void RK3_Step2_functional2D<T>::getTypeOfModification(std::vector<modif::ModifT>& modified) const {
    modified[0] = modif::nothing; // phi_n
    modified[1] = modif::nothing; // phi_1
    modified[2] = modif::nothing; // phi_1_adv
    modified[3] = modif::staticVariables; // phi_2
}

template<typename T>
BlockDomain::DomainT RK3_Step2_functional2D<T>::appliesTo() const {
    return BlockDomain::bulkAndEnvelope; // Everything is local, no communication needed.
}

/* ******** RK3_Step3_functional2D ****************************************** */

template<typename T>
void RK3_Step3_functional2D<T>::process (
        Box2D domain, std::vector<ScalarField2D<T>*> fields )
{
    PLB_PRECONDITION( fields.size() == 4 );
    ScalarField2D<T>& phi_n = *fields[0];
    ScalarField2D<T>& phi_2 = *fields[1];
    ScalarField2D<T>& phi_2_adv = *fields[2];
    ScalarField2D<T>& volfracField_RK = *fields[3];
    Dot2D offset_phi_2     = computeRelativeDisplacement(phi_n, phi_2);
    Dot2D offset_phi_2_adv  = computeRelativeDisplacement(phi_n, phi_2_adv);
    Dot2D offset_volfracField_RK      = computeRelativeDisplacement(phi_n, volfracField_RK);
    for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY) {

                volfracField_RK.get(iX+offset_volfracField_RK.x,iY+offset_volfracField_RK.y)
                    = 1./3. * phi_n.get(iX,iY) +
                      2./3. * phi_2.get(iX+offset_phi_2.x,iY+offset_phi_2.y) +
                      2./3. * phi_2_adv.get(iX+offset_phi_2_adv.x,iY+offset_phi_2_adv.y);

        }
    }
}

template<typename T>
RK3_Step3_functional2D<T>* RK3_Step3_functional2D<T>::clone() const {
    return new RK3_Step3_functional2D<T>(*this);
}

template<typename T>
void RK3_Step3_functional2D<T>::getTypeOfModification(std::vector<modif::ModifT>& modified) const {
    modified[0] = modif::nothing; // phi_n
    modified[1] = modif::nothing; // phi_2
    modified[2] = modif::nothing; // phi_2_adv
    modified[3] = modif::staticVariables; // volfracField_RK
}

template<typename T>
BlockDomain::DomainT RK3_Step3_functional2D<T>::appliesTo() const {
    return BlockDomain::bulkAndEnvelope; // Everything is local, no communication needed.
}

}

#endif
